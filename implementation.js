document.MiKandiGames = {};
document.MiKandiGames._load_complete = false;
document.MiKandiGames._showing_more_games = false;
document.MiKandiGames._ad_abuse_preventer = false;
document.MiKandiGames._sloppy_mutex = false;
document.MiKandiGames._init_called = false;
$("#mkg_loading_screen").ready(function() {
	console.log("Low level page initialization");
	document.MiKandiGames._game_id = document.getElementById("mkg_game_id").innerHTML;
    document.MiKandiGames._publisher_id = document.getElementById("mkg_publisher_id").innerHTML;
    $("#mkg_close_more_games").click( function(e) {
		console.log("hiding more games");
		e.preventDefault();
		$("#mkg_more_games").hide();
		$("#canvas").show();
		document.MiKandiGames._showing_more_games = false;
		return false;
    });
	//setTimeout('document.MiKandiGames.insertGMScript()', 5000);
	setTimeout('document.MiKandiGames.justInCaseDismiss()', 15000);
});
document.MiKandiGames.justInCaseDismiss = function()
{
	if (document.MiKandiGames._init_called == false)
	{
		console.log('dismissing loadscreen without call from GM.');
		document.MiKandiGames.Init();
	}
}
document.MiKandiGames.insertGMScript = function()
{
	$.ajax({
		url: 'html5game/widget_test.js?cache-buster='+(new Date()).getTime().toString().substr(0, 12),
		dataType: 'script',
		async: true,
		success: function(data)
		{	
			console.log('script loaded.');
			document.write('<script type="text/javascript">' + data + ';</script>');
		}});
	
	return;
	//document.write('<script type="text/javascript" src="html5game/widget_test.js?cache-buster='+(new Date()).getTime().toString().substr(0, 12)+'"></script>');
	//return;
}
document.MiKandiGames.Init = function()
{
	console.log("Initializing the game widget");
    console.log("The game is " + document.MiKandiGames._game_id + " running on publisher " + document.MiKandiGames._publisher_id);
	document.MiKandiGames._init_called = true;
	document.MiKandiGames.dismissLoadScreen();
    return "success";
}
document.MiKandiGames.Deinit = function()
{
    console.log("De-initializing the game widget");
    return "success";
}
document.MiKandiGames.dismissLoadScreen = function()
{
	if (document.MiKandiGames._load_complete) return "success";
    document.MiKandiGames._load_complete = true;
    $("#mkg_loading_screen").hide();
    $("#canvas").show();
    console.log("loading complete.");
	return "success";
}
document.MiKandiGames.startShowingAds = function(_where)
{
    // if (!(typeof(where) === "object") || where.length%3) {
		// console.log("WTF dude?");
		// return "failure";
    // }
	// console.log(_where);
	// var where = [];
	// for (var i=0; i<_where.length; i+=3)
	// {
		// var temp =
		// {
			// type: _where[i],
			// x: _where[i+1],
			// y: _where[i+2]
		// };
		// where.push(temp);
	// }
	var where = _where;
	for (var i=0; i<where.length; i++)
	{
		console.log("Object " + i + ":: " + where[i].type + ", " + where[i].x + ", " + where[i].y);
	}
	if (document.MiKandiGames._ad_abuse_preventer == true)
    {
        console.log("Trying to show ads before cooldown.");
        return "failure";
    }
    else
    {
        document.MiKandiGames._ad_abuse_preventer = true;
        setTimeout('document.MiKandiGames._ad_abuse_preventer = false', 5000);
    }
    if (!(typeof(where) === "object" && where.length !== undefined)) {
	where = [where];
    }
    console.log("showing ads");
    for (var i = 0; i < where.length; i += 1) {
		console.log(" Can show an ad at '" + where[i] + "'");
		$('#gm4html5_div_id').append(
		"<iframe id=\"mkg_on_demand_ad_iframe\"  class=\"mkg_ad_type_" + where[i].type +
		" mkg_generic_ad_class\" src=\"http://as.sexad.net/as/pu?p=mikandi&v=3954\" seamless frameborder=\"0\" style=\"position: absolute; top: " + where[i].y + "px; left: " + where[i].x + "px; z-index: 1002;\"></iframe>"
		);
    }
}
document.MiKandiGames.stopShowingAds = function()
{
	console.log("hide ads");
	$('.mkg_generic_ad_class').remove();
}
document.MiKandiGames.moreGames = function()
{
    if (document.MiKandiGames._showing_more_games === true) {
		console.log("Got an unexpected call to moreGames");
		return "success";
    }
    document.MiKandiGames._showing_more_games = true;
    $("#canvas").hide();
    $("#mkg_more_games").show();
    console.log("more games");
    return "success";
}
