/**
 * Created by gbrousseau on 12/30/14.
 */
var widget = document.getElementById('widget');
var query = widget.src;
var game = getQuerystring('game','fap-ninja');
var tID = getQuerystring('tID', '1587633');
var platform = false;

if((navigator.userAgent.match(/iPhone/i)) ||
    (navigator.userAgent.match(/iPod/i)) ||
    (navigator.userAgent.match(/iPad/i))) {
    platform = true;
    }


function getQuerystring(key, default_)
{
    if (default_==null) default_="";
    key = key.replace(/[\\[]/,"\\\\\\[").replace(/[\\]]/,"\\\\\\]");
    var regex = new RegExp("[\\\\?&]"+key+"=([^&#]*)");
    var qs = regex.exec(query);
    if(qs == null)
        return default_;
    else
        return qs[1];
}

/**  variable name  path   ***/
var games = [];
games['save-squirt'] = {path: "save-squirt",meta_description: "games, mikandi, squirt", name: "squirt", height: 640, width: 960};
games['8-bit-dick'] = {path: "8-bit-dick",meta_description: "games, mikandi, 8-bit-dick", name: "8-bit-dick", height: 640, width: 960};
games['fap-piano-shemale'] = {path: "fap-piano-shemale",meta_description: "games, mikandi, fap-piano-shemale", name: "fap-piano-shemale", height: 640, width: 960};
games['fap-piano-camgirls'] = {path: "fap-piano-camgirls",meta_description: "games, mikandi, fap-piano-camgirls", name: "fap-piano-camgirls", height: 640, width: 960};
games['strip-ninja'] = {path: "strip-ninja",meta_description: "games, mikandi, strip-ninja", name: "strip-ninja", height: 640, width: 960};
games['fap-tap'] = {path: "fap-tap",meta_description: "games, mikandi, fap-tap", name: "fap-tap", height: 640, width: 960};
games['fap-ninja'] = {path: "fap-ninja",meta_description: "games, mikandi, fap-ninja", name: "fap-ninja", height: 640, width: 960};
games['test-game-ad'] = {path: "test-game-ad",meta_description: "games, mikandi, test-game-ad", name: "test-game", height: 640, width: 960};

    (function () {
        var scriptName = "widget.js"; //name of this script, used to get reference to own tag
        var jQuery; //noconflict reference to jquery
        var jqueryPath = "http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js";
        var jqueryVersion = "2.1.1";

        /******** Get reference to self (scriptTag) *********/
        var allScripts = document.getElementsByTagName('script');
        var targetScripts = [];
        for (var i in allScripts) {
            var name = allScripts[i].src
            if (name && name.indexOf(scriptName) > 0)
                targetScripts.push(allScripts[i]);
        }
        /******** helper function to load external scripts *********/
        function loadScript(src, onLoad) {
            var script_tag = document.createElement('script');
            script_tag.setAttribute("type", "text/javascript");
            script_tag.setAttribute("src", src);

            if (script_tag.readyState) {
                script_tag.onreadystatechange = function () {
                    if (this.readyState == 'complete' || this.readyState == 'loaded') {
                        onLoad();
                    }
                };
            } else {
                script_tag.onload = onLoad;
            }
            (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(script_tag);
        }

        /******** helper function to load external css *********/
        function loadCss(href) {
            var link_tag = document.createElement('link');
            link_tag.setAttribute("type", "text/css");
            link_tag.setAttribute("rel", "stylesheet");
            link_tag.setAttribute("href", href);
            (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(link_tag);
        }

        /******** load jquery into 'jQuery' variable then call main ********/
        if (window.jQuery === undefined || window.jQuery.fn.jquery !== jqueryVersion) {
            loadScript(jqueryPath, initjQuery);
        } else {
            initjQuery();
        }

        function initjQuery() {
            jQuery = window.jQuery.noConflict(true);
            main();
        }

        /******** starting point for your widget ********/
        function main() {
            jQuery(document).ready(function ($) {
                    setInterval(resetIframe, 250);
                    $(window).on('touchstart', function (e) {
                            end_ad("#fullscreen_banner");
                            toggleFullScreen();
                    });

                    $(window).on('click', function (e) {
                        end_ad("#fullscreen_banner");
                        toggleFullScreen();
                    });

                
                //loadScript("url", function() { /* loaded */ });
				window.gamefeed = undefined;
				
				/*
				** Implementation using gamefeed. Obsolete.
				*/
				/*
				$.ajax({
					url: "gamefeed.js",
					dataType: "json",
					async: false,
					success: function(data) {
					    document.write('<div id="fullscreen_banner" class="mikandi-ad"><a class="boxclose"></a></div>');
						window.gamefeed = data;
						document.write('<iframe id="game" src="http://game-dev.mikandi.com/games/' + window.gamefeed['games'][game].urlencname + '/index.php?tID='+tID+'" height="640" width="960" seamless="true" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen="true" webkit-playsinline="true" frameborder="0" scrolling="no"><\/iframe>');
						loadCss("https://gameservice-a.mikandicdn.com/widget.css");
					}
				});
				//access it like: window.gamefeed['games'][game].title
				*/
				loadCss("https://gameservice-a.mikandicdn.com/widget.css");
                function resetIframe() {
                    if ((window.fullScreen) ||
                        (window.innerWidth == screen.width && window.innerHeight == screen.height)) {
                        return;
                    } else {
                        if( ! platform){
                            start_ad("#fullscreen_banner");
                        }
                    }
                    $("#fullscreen_banner").width($(document).width());
                    $("#fullscreen_banner").height($(document).height());
                }

                $('a.boxclose').on('click', function () {
                    end_ad("#" + $(this).parent().prop('id'));
                });

                function start_ad(e) {
                    $(e).show();
                }

                function end_ad(e) {
                    $(e).hide();
                }


            });
        }
    })();

    // document.write('<iframe id="game" src="https://gameservice-a.mikandicdn.com/' + games[game].path + '/index.html" height="640" width="960" seamless="true" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen="true" webkit-playsinline="true" frameborder="0" scrolling="no"><\/iframe>');
	// document.write('<iframe id="game" src="https://game-dev.mikandi.com/games/' + games[game].path + '/index.php" height="640" width="960" seamless="true" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen="true" webkit-playsinline="true" frameborder="0" scrolling="no"><\/iframe>');
	document.write('<iframe id="game" src="http://game-dev.mikandi.com/games/' + games[game].path + '/index.php?tID='+tID+'" height="640" width="960" seamless="true" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen="true" webkit-playsinline="true" frameborder="0" scrolling="no"><\/iframe>');
    document.write('<div id="fullscreen_banner" class="mikandi-ad"><a class="boxclose"></a></div>');

    function toggleFullScreen() {
        if (!document.mozFullScreen && !document.webkitFullScreen) {
            if (!document.fullscreenElement &&    // alternative standard method
                !document.mozFullScreenElement && !document.webkitFullscreenElement && !document.msFullscreenElement) {  // current working methods
                if (document.documentElement.requestFullscreen) {
                    document.documentElement.requestFullscreen();
                } else if (document.documentElement.msRequestFullscreen) {
                    document.documentElement.msRequestFullscreen();
                } else if (document.documentElement.mozRequestFullScreen) {
                    document.documentElement.mozRequestFullScreen();
                } else if (document.documentElement.webkitRequestFullscreen) {
                    document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
                }
            }
        }
    }