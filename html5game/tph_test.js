var mkg;
function mkg_init()
{
	if (typeof(document) === "object" && typeof(document.MiKandiGames) === "object")
	{
		mkg = document.MiKandiGames;
		if (typeof(mkg.Init()) === "function")
			return mkg.Init();
		else
			return "failure";
	}
	else
	{
		mkg = {};
		return "failure";
	}	
}

function mkg_dismiss_load_screen()
{
	if (typeof(mkg.dismissLoadScreen) === "function")
	{
		return mkg.dismissLoadScreen();
	}
	else
		return "failure";
} 

function mkg_start_showing_ads(where)
{

    if (!(typeof(where) === "object") || where.length%3) {
		return "failure";
    }
	
	if (typeof(mkg.startShowingAds) === "function")
	{
		var placement = [];
		
		for (var i=0; i<where.length; i+=3)
		{
			var temp =
			{
				type: where[i],
				x: where[i+1],
				y: where[i+2]
			};
			placement.push(temp);
		}
		
		return mkg.startShowingAds(placement);
	}
	else
		return "failure";
} 

function mkg_stop_showing_ads()
{
	
	if (typeof(mkg.stopShowingAds) === "function")
	{
		return mkg.stopShowingAds();
	}
	else
		return "failure";
} 

function mkg_more_games()
{
	
	if (typeof(mkg.moreGames) === "function")
	{
		return mkg.moreGames();
	}
	else
		return "failure";
} 

function mkg_deinit()
{
	if (typeof(document) === "object" && typeof(document.MiKandiGames) === "object")
	{
		if (typeof(mkg.Deinit()) === "function")
			return mkg.Deinit();
		else
			return "failure";
	}
}